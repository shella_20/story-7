from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from . import models
from selenium import webdriver
from . import views

from selenium.webdriver.chrome.options import Options

    
class UnitTest(TestCase):
    def setUp(self):
        self.client = Client()
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        # self.browser = webdriver.Chrome(chrome_options=options)
        self.browser = webdriver.Chrome(chrome_options=options)
        

    def tearDown(self):
        self.browser.close()

    def test_halo_text(self):
        # self.browser = webdriver.Chrome('Status/chrome_77_driver/chromedriver.exe')
        self.browser.get('http://localhost:8000')
        #self.browser.get('http://shella-story7-ppw.herokuapp.com')

        halo = self.browser.find_element_by_class_name('test_hello').text
        self.assertEquals(halo, "Hello! I'm Shella")
        # self.browser.close()

    def test_title(self):
        # self.browser = webdriver.Chrome('Status/chrome_77_driver/chromedriver.exe')
        self.browser.get('http://localhost:8000')
        #self.browser.get('http://shella-story7-ppw.herokuapp.com')

        self.assertEquals(self.browser.title, 'Story 7 PPW')
        # self.browser.close()

    def test_template_contain_Activity_Experience_Achievement(self):
        request = HttpRequest()
        response = views.index(request)
        self.assertContains(response, 'Activity')
        self.assertContains(response, 'Experience')
        self.assertContains(response, 'Achievement')
    
        #test url '/' exist or not
    def test_url_exist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

        #test url using status view function
    def test_calling_right_views_function(self):
        found_views = resolve('/')
        self.assertEqual(found_views.func, views.index)

        #test '/' using templates index.html
    def test_using_right_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'homepage.html')
