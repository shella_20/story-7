from django.urls import path, include
from . import views

app_name = 'Story7_app'

urlpatterns = [
    path('', views.index),
]
